# Test technique React - ekWateur

## Lancement

'yarn start'

## Interface

Une page d'accueil pour accéder à sa consommation

Une page pour visualiser sa consommation avec :

- Un bloc de filtres par énergie / année / ordre de tri
- Un tableau avec les données
- La liste de mes PDL

## Packages

Le choix de styled-components est inspiré de votre stack mentionnant du CSS in JS.

J'ai priviligié date-fns à moment.js qui n'est désormais plus maintenu.

Le tri est volontairement fait en JS pur, j'aurais aussi pu utiliser le orderBy de lodash, par exemple.

## Tests

'yarn test'

Tests de deux composants et deux fonctions
