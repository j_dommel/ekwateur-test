const radii = [4, 8, 16, 24, 32];

export default {
	xs: radii[0],
	s: radii[1],
	m: radii[2],
	l: radii[3],
	xl: radii[4],
};
