export default {
	primary: "#4BAA98",
	secondary: "#F6B033",
	white: "#FFFFFF",
	font: {
		dark: "#494949",
	},
	error: "#E81E29",
};
