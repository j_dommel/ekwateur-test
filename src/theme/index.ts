import colors from "./colors";
import radii from "./radii";

const theme = {
	colors,
	radii,
};

export default theme;
