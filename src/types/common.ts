export interface IPod {
	id: string;
	createdAt: string;
	pointOfDelivery: string | any[];
}

export interface IIndex {
	id: string;
	meterId: string;
	createdAt: string;
	indexHigh: number;
	indexLow?: number;
	meter: IPod;
}

export type EnergyType = "gas" | "electricity";

export type SortType =
	| "recentFirst"
	| "olderFirst"
	| "lowerFirst"
	| "higherFirst";
