import { isValid, getYear } from "date-fns";
import { IIndex, SortType } from "./types/common";

export const getAvailableYears = (podIndexes?: IIndex[]) => {
	if (!podIndexes) {
		return undefined;
	}

	// map indexes to list all years
	const availableYears = podIndexes.reduce((arr: number[], curr) => {
		const currYear = getYear(new Date(curr.createdAt));

		if (!arr.includes(currYear)) {
			arr = [...arr, currYear];
		}

		return arr;
	}, []);

	return availableYears;
};

export const getSortedIndexes = (
	selectedSort: SortType,
	podIndexes?: IIndex[],
	selectedYear?: number
) => {
	if (!podIndexes) {
		return undefined;
	}

	let indexes = [...podIndexes];

	// filter by year
	if (selectedYear) {
		indexes = indexes.filter(
			(pod) => getYear(new Date(pod.createdAt)) === selectedYear
		);
	}

	// sort according to chosen option
	if (selectedSort === "olderFirst" || selectedSort === "recentFirst") {
		indexes = indexes.sort((a, b) => {
			const dateA = new Date(a.createdAt).getTime();
			const dateB = new Date(b.createdAt).getTime();
			return selectedSort === "olderFirst" ? dateA - dateB : dateB - dateA;
		});
	} else if (selectedSort === "higherFirst" || selectedSort === "lowerFirst") {
		indexes = indexes.sort((a, b) => {
			const indexA = a.indexHigh + (a.indexLow || 0);
			const indexB = b.indexHigh + (b.indexLow || 0);
			return selectedSort === "lowerFirst" ? indexA - indexB : indexB - indexA;
		});
	}

	return indexes;
};

export const filterInvalidIndexes = (podIndexes?: IIndex[]) => {
	// API can send invalid dates
	// preventing crash
	let indexes = podIndexes?.filter((pod) => isValid(new Date(pod.createdAt)));

	return indexes;
};

export const getIndexEvolution = (
	index: number,
	indexId: string,
	chronologicalIndexes: IIndex[]
) => {
	// find previous chronological index and compare
	const indexIndexInArray = chronologicalIndexes.findIndex(
		(index) => index.id === indexId
	);
	const chronologicalPreviousIndex =
		indexIndexInArray !== 0
			? chronologicalIndexes[indexIndexInArray - 1].indexHigh +
			  (chronologicalIndexes[indexIndexInArray - 1].indexLow || 0)
			: undefined;

	return chronologicalPreviousIndex !== undefined
		? Math.sign(index - chronologicalPreviousIndex)
		: undefined;
};
