import styled from "styled-components";

const BlockContainer = styled.div`
	box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
	border-radius: ${({ theme }) => theme.radii.l}px;
	margin-bottom: 32px;
`;
export default BlockContainer;
