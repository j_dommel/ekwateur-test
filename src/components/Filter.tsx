import styled from "styled-components";
import Text from "./Text";

const FilterSelect = styled.select`
	border-radius: ${({ theme }) => theme.radii.l}px;
	color: ${({ theme }) => theme.colors.white};
	font-size: 16px;
	font-family: "Avenir Medium";
	padding: 8px;
	border-width: 0px;
	background-color: ${({ theme }) => theme.colors.secondary};
`;

const FilterText = styled(Text)`
	font-size: 19px;
	text-align: center;
	margin-bottom: 8px;
`;

export { FilterSelect, FilterText };
