import React, { FunctionComponent } from "react";
import { SortType } from "../types/common";
import { FilterSelect, FilterText } from "./Filter";

interface SortSelectProps {
	onChangeSort: (type: SortType) => void;
	selectedSort: SortType | undefined;
}

const SortSelect: FunctionComponent<SortSelectProps> = ({
	onChangeSort,
	selectedSort,
}) => {
	return (
		<>
			<FilterText>Trier par :</FilterText>
			<FilterSelect
				value={selectedSort}
				onChange={(e) => onChangeSort(e.target.value as SortType)}
			>
				<option value={"recentFirst"}>Les + récents</option>
				<option value={"olderFirst"}>Les + anciens</option>
				<option value={"lowerFirst"}>Les - élevés</option>
				<option value={"higherFirst"}>Les + élevés</option>
			</FilterSelect>
		</>
	);
};

export default SortSelect;
