import React, { FunctionComponent } from "react";
import { FilterSelect, FilterText } from "./Filter";

interface YearSelectProps {
	onChangeYear: (year: number) => void;
	availableYears: number[];
	selectedYear: number | undefined;
}

const YearSelect: FunctionComponent<YearSelectProps> = ({
	onChangeYear,
	availableYears,
	selectedYear,
}) => {
	return (
		<div>
			<FilterText>Par année :</FilterText>
			<FilterSelect
				data-testid="year-select"
				value={selectedYear}
				onChange={(e) => onChangeYear(Number(e.target.value))}
			>
				<option value={0}>Toutes les années</option>
				{availableYears.map((year) => (
					<option key={year} value={year}>
						{year}
					</option>
				))}
			</FilterSelect>
		</div>
	);
};

export default YearSelect;
