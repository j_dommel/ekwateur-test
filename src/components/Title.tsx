import styled from "styled-components";
import Text from "./Text";

const Title = styled(Text)`
	font-size: 40px;
	padding: 0px 16px;
	color: ${({ theme }) => theme.colors.primary};
	@media (max-width: 600px) {
		font-size: 32px;
	}
`;

export default Title;
