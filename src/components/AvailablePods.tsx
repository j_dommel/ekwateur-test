import React, { FunctionComponent } from "react";
import styled from "styled-components";
import { IPod } from "../types/common";
import BlockContainer from "./BlockContainer";
import Text from "./Text";
import { FilterText } from "./Filter";

const ListRow = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	margin: 8px;
`;

const AvailablePodsContainer = styled(BlockContainer)`
	padding: 16px;
`;

const IdContainer = styled.div`
	width: 24px;
	height: 24px;
	border-radius: 12px;
	display: flex;
	align-items: center;
	justify-content: center;
	background-color: ${({ theme }) => theme.colors.secondary};
	margin-right: 8px;
`;

const IdText = styled(Text)`
	color: ${({ theme }) => theme.colors.white};
	padding: 0;
`;

interface AvailablePodsProps {
	podList: IPod[];
}

const AvailablePods: FunctionComponent<AvailablePodsProps> = ({ podList }) => {
	return (
		<AvailablePodsContainer>
			<FilterText>Mes points de livraison</FilterText>
			{podList.map((pod) => (
				<ListRow key={pod.id}>
					<IdContainer>
						<IdText>{pod.id}</IdText>
					</IdContainer>
					<Text>{pod.pointOfDelivery}</Text>
				</ListRow>
			))}
		</AvailablePodsContainer>
	);
};

export default AvailablePods;
