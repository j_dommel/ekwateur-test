import React, { FunctionComponent } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import Text from "./Text";

const BackLink = styled(Link)`
	text-decoration: none;
	display: flex;
	align-items: center;
	flex-direction: row;
	background-color: ${({ theme }) => theme.colors.secondary};
	padding: 8px;
	border-radius: ${({ theme }) => theme.radii.l}px;
	margin-bottom: 24px;
	height: 32px;
`;

const BackIcon = styled.i`
	color: ${({ theme }) => theme.colors.white};
	margin-right: 8px;
`;

const BackText = styled(Text)`
	color: ${({ theme }) => theme.colors.white};
`;

const BackButton: FunctionComponent = () => {
	return (
		<BackLink to="/">
			<BackIcon className="fas fa-arrow-left" />
			<BackText>Retour</BackText>
		</BackLink>
	);
};

export default BackButton;
