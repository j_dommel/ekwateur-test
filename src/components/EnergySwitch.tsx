import React, { FunctionComponent } from "react";
import styled from "styled-components";
import { EnergyType } from "../types/common";
import { FilterText } from "./Filter";
import Text from "./Text";

interface EnergySwitchProps {
	onChange: (type: EnergyType) => void;
	selectedType: EnergyType;
}

interface ChoiceButtonProps {
	isSelected: boolean;
	type: EnergyType;
}

interface ChoiceTextProps {
	isSelected: boolean;
}

const SwitchContainer = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	margin-top: 4px;
`;

const ChoiceButton = styled.a<ChoiceButtonProps>`
	position: relative;
	height: 64px;
	width: 100px;
	diplay: flex;
	align-items: center;
	justify-content: center;
	cursor: pointer;
	background-color: ${({ isSelected, theme }) =>
		isSelected ? theme.colors.primary : theme.colors.white};
	border-radius: ${({ type }) =>
		type === "gas" ? "8px 0px 0px 8px" : "0px 8px 8px 0px"};
	border: 1px solid ${({ theme }) => theme.colors.primary};
	border-width: ${({ type }) =>
		type === "gas" ? "1px 0px 1px 1px" : "1px 1px 1px 0px"};
`;

const ChoiceText = styled(Text)<ChoiceTextProps>`
	color: ${({ isSelected, theme }) =>
		isSelected ? theme.colors.white : theme.colors.primary};
	font-size: 19px;
	text-align: center;
`;

const EnergySwitch: FunctionComponent<EnergySwitchProps> = ({
	onChange,
	selectedType,
}) => {
	return (
		<>
			<FilterText>Choisissez le compteur :</FilterText>
			<SwitchContainer>
				<ChoiceButton
					isSelected={selectedType === "gas"}
					type="gas"
					onClick={() => onChange("gas")}
				>
					<ChoiceText isSelected={selectedType === "gas"}>Gaz</ChoiceText>
				</ChoiceButton>
				<ChoiceButton
					isSelected={selectedType === "electricity"}
					type="electricity"
				>
					<ChoiceText
						isSelected={selectedType === "electricity"}
						onClick={() => onChange("electricity")}
					>
						Electricité
					</ChoiceText>
				</ChoiceButton>
			</SwitchContainer>
		</>
	);
};

export default EnergySwitch;
