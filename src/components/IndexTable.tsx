import React, { FunctionComponent } from "react";
import styled from "styled-components";
import { IIndex } from "../types/common";
import BlockContainer from "./BlockContainer";
import IndexRow from "./IndexRow";
import IndexTableHeader from "./IndexTableHeader";
import Text from "./Text";

interface IndexTableProps {
	indexes: IIndex[];
	chronologicalIndexes: IIndex[];
}

const TableContainer = styled(BlockContainer)`
	@media (max-width: 600px) {
		width: 100%;
		border-radius: 0px;
	}
`;

const IndexTable: FunctionComponent<IndexTableProps> = ({
	indexes,
	chronologicalIndexes,
}) => {
	const hasLowIndexes = indexes.some((pod) => Boolean(pod.indexLow));

	return (
		<TableContainer>
			<IndexTableHeader hasLowIndexes={hasLowIndexes} />
			{indexes.map((podIndex, arrayIndex) => (
				<IndexRow
					key={podIndex.id}
					indexHigh={podIndex.indexHigh}
					indexLow={podIndex.indexLow}
					indexId={podIndex.id}
					date={podIndex.createdAt}
					chronologicalIndexes={chronologicalIndexes}
					isLast={arrayIndex === indexes.length - 1}
				/>
			))}
			{indexes.length === 0 && <Text>Pas de données selon ces critères</Text>}
		</TableContainer>
	);
};

export default IndexTable;
