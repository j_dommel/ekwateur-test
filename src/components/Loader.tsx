import React, { FunctionComponent } from "react";
import styled from "styled-components";
import Text from "./Text";

const LoaderText = styled(Text)`
	font-size: 24px;
	color: ${({ theme }) => theme.colors.primary};
`;

const Loader: FunctionComponent = () => {
	return (
		<LoaderText>
			Patientez quelques secondes, nous cherchons vos données...
		</LoaderText>
	);
};

export default Loader;
