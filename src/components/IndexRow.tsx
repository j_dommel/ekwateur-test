import React, { FunctionComponent } from "react";
import styled from "styled-components";
import { format } from "date-fns";
import { IIndex } from "../types/common";
import { getIndexEvolution } from "../utils";
import Text from "./Text";

interface IndexRowProps {
	indexHigh: number;
	indexLow?: number;
	indexId: string;
	date: string;
	chronologicalIndexes: IIndex[];
	isLast: boolean;
}

interface ILastItem {
	isLast?: boolean;
}

export const RowContainer = styled.div<ILastItem>`
	display: flex;
	flex-direction: row;
	align-items: center;
	width: 600px;
	height: 56px;
	border-bottom: ${({ isLast }) => (isLast ? 0 : 1)}px solid
		${({ theme }) => theme.colors.primary};
	@media (max-width: 600px) {
		width: 100%;
	}
`;

const Column = styled.div<{ isHeader?: boolean }>`
	display: flex;
	align-items: center;
	justify-content: center;
	background-color: ${({ isHeader, theme }) =>
		isHeader ? theme.colors.primary : theme.colors.white};
	height: 100%;
`;

export const IdColumn = styled(Column)<ILastItem>`
	width: 15%;
	border-bottom-left-radius: ${({ theme, isLast }) =>
		isLast ? theme.radii.l : 0}px;
	background-color: ${({ theme }) => theme.colors.primary};
	@media (max-width: 600px) {
		border-radius: 0px;
	}
`;

export const IndexColumn = styled(Column)`
	width: 35%;
`;

export const DateColumn = styled(Column)`
	width: 35%;
`;

export const EvolutionColumn = styled(Column)<ILastItem>`
	width: 15%;
	border-bottom-right-radius: ${({ theme, isLast }) =>
		isLast ? theme.radii.l : 0}px;
`;

export const DataText = styled(Text)<{ isId?: boolean }>`
	text-align: center;
	color: ${({ theme, isId }) =>
		isId ? theme.colors.white : theme.colors.font.dark};
	margin: 0;
	@media (max-width: 374px) {
		font-size: 14px;
	}
`;

const EvolutionIcon = styled.i<{ evolution?: number }>`
	color: ${({ evolution, theme }) =>
		evolution
			? evolution === 1
				? theme.colors.error
				: "green"
			: theme.colors.font.dark};
`;

const IndexRow: FunctionComponent<IndexRowProps> = ({
	indexHigh,
	indexLow,
	indexId,
	date,
	chronologicalIndexes,
	isLast,
}) => {
	const formattedDate = format(new Date(date), "dd/MM/yy hh:mm");

	const evolutionIndex = indexHigh + (indexLow || 0);

	const evolution = getIndexEvolution(
		evolutionIndex,
		indexId,
		chronologicalIndexes
	);

	return (
		<RowContainer isLast={isLast}>
			<IdColumn isLast={isLast}>
				<DataText isId>{indexId}</DataText>
			</IdColumn>
			<IndexColumn>
				<DataText>
					{indexHigh}
					{indexLow && ` / ${indexLow}`}
				</DataText>
			</IndexColumn>
			<DateColumn>
				<DataText>{formattedDate}</DataText>
			</DateColumn>
			<EvolutionColumn isLast={isLast}>
				<EvolutionIcon
					evolution={evolution}
					className={
						evolution !== undefined
							? evolution === 1
								? "fas fa-arrow-up"
								: "fas fa-arrow-down"
							: "fas fa-minus"
					}
				/>
			</EvolutionColumn>
		</RowContainer>
	);
};

export default IndexRow;
