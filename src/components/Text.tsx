import styled from "styled-components";

const Text = styled.p`
	font-family: "Avenir Medium", sans-serif;
	color: ${({ theme }) => theme.colors.font.dark};
	text-align: center;
`;

export default Text;
