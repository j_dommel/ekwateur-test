import React, { FunctionComponent } from "react";
import styled from "styled-components";
import {
	RowContainer,
	IdColumn,
	IndexColumn,
	DateColumn,
	EvolutionColumn,
	DataText,
} from "./IndexRow";
import Text from "./Text";

const HeaderDataText = styled(DataText)`
	color: ${({ theme }) => theme.colors.white};
	font-weight: 500;
`;

const LeftColumn = styled(IdColumn)`
	border-top-left-radius: ${({ theme }) => theme.radii.l}px;
	@media (max-width: 600px) {
		border-radius: 0px;
	}
`;

const HeaderIndexColumn = styled(IndexColumn)`
	flex-direction: column;
	height: 56px;
	position: relative;
`;

const RightColumn = styled(EvolutionColumn)`
	border-top-right-radius: ${({ theme }) => theme.radii.l}px;
	@media (max-width: 600px) {
		border-radius: 0px;
	}
`;

const IndexLegend = styled(Text)`
	font-size: 12px;
	color: ${({ theme }) => theme.colors.white};
	margin: 0;
	position: absolute;
	bottom: 4px;
	@media (max-width: 374px) {
		display: none;
	}
`;

interface IndexTableHeaderProps {
	hasLowIndexes: boolean;
}

const IndexTableHeader: FunctionComponent<IndexTableHeaderProps> = ({
	hasLowIndexes,
}) => {
	return (
		<RowContainer>
			<LeftColumn isHeader>
				<HeaderDataText>Index n°</HeaderDataText>
			</LeftColumn>
			<HeaderIndexColumn isHeader>
				<HeaderDataText>Relevé</HeaderDataText>
				{hasLowIndexes && <IndexLegend>H.Pleines /H. Creuses</IndexLegend>}
			</HeaderIndexColumn>
			<DateColumn isHeader>
				<HeaderDataText>Date de relevé</HeaderDataText>
			</DateColumn>
			<RightColumn isHeader>
				<HeaderDataText>Evol.</HeaderDataText>
			</RightColumn>
		</RowContainer>
	);
};

export default IndexTableHeader;
