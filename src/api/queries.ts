import axios from "axios";
import { useEffect, useState } from "react";
import { IIndex, IPod } from "../types/common";
import { filterInvalidIndexes } from "../utils";

const API_URL = "https://5e9ed3cdfb467500166c47bb.mockapi.io/api/v1/";

export const usePodList = () => {
	const [podList, setPodList] = useState<IPod[]>();

	const getPodList = async () => {
		const response = await axios.get(`${API_URL}meter/`);

		// filter to display only item with a pointOfDelivery key
		const availablePods = response.data.filter(
			(pod: IPod) => typeof pod.pointOfDelivery === "string"
		);

		setPodList(availablePods);
	};

	useEffect(() => {
		getPodList();
	}, []);

	return { podList };
};

export const useGasIndexes = () => {
	const [gasIndexes, setGasIndexes] = useState<IIndex[]>();

	const getGasIndexes = async () => {
		const response = await axios.get(`${API_URL}meter/1/gas`);
		setGasIndexes(response.data);
	};

	useEffect(() => {
		getGasIndexes();
	}, []);

	return { gasIndexes: filterInvalidIndexes(gasIndexes) };
};

export const useElectricityIndexes = () => {
	const [electricityIndexes, setElectricityIndexes] = useState<IIndex[]>();

	const getElectricityIndexes = async () => {
		const response = await axios.get(`${API_URL}meter/2/electricity`);
		setElectricityIndexes(response.data);
	};

	useEffect(() => {
		getElectricityIndexes();
	}, []);

	return { electricityIndexes: filterInvalidIndexes(electricityIndexes) };
};
