import React, { FunctionComponent, useState, useEffect } from "react";
import styled from "styled-components";
import {
	useElectricityIndexes,
	useGasIndexes,
	usePodList,
} from "../api/queries";
import EnergySwitch from "../components/EnergySwitch";
import IndexTable from "../components/IndexTable";
import Loader from "../components/Loader";
import SortSelect from "../components/SortSelect";
import YearSelect from "../components/YearSelect";
import { EnergyType, SortType } from "../types/common";
import { getSortedIndexes, getAvailableYears } from "../utils";
import ScreenContainer from "../components/ScreenContainer";
import Title from "../components/Title";
import BlockContainer from "../components/BlockContainer";
import AvailablePods from "../components/AvailablePods";
import BackButton from "../components/BackButton";

const FilterBlockContainer = styled(BlockContainer)`
	padding: 16px;
	display: flex;
	flex-direction: column;
	align-items: center;
`;

const MeterInfos: FunctionComponent = () => {
	const [selectedEnergy, setSelectedEnergy] = useState<EnergyType>("gas");
	const [selectedYear, setSelectedYear] = useState<number>(0);
	const [sortType, setSortType] = useState<SortType>("recentFirst");

	const { podList } = usePodList();
	const { gasIndexes } = useGasIndexes();
	const { electricityIndexes } = useElectricityIndexes();

	useEffect(() => {
		// reset year filter when changing energy type
		setSelectedYear(0);
	}, [selectedEnergy]);

	const selectedIndexes =
		selectedEnergy === "electricity" ? electricityIndexes : gasIndexes;

	const indexes = getSortedIndexes(sortType, selectedIndexes, selectedYear);

	const chronologicalIndexes = getSortedIndexes(
		"olderFirst",
		selectedIndexes,
		selectedYear
	);

	const availableYears = getAvailableYears(selectedIndexes);

	return (
		<ScreenContainer>
			<Title>Ma consommation</Title>
			<FilterBlockContainer>
				<EnergySwitch
					onChange={setSelectedEnergy}
					selectedType={selectedEnergy}
				/>
				{availableYears && (
					<YearSelect
						availableYears={availableYears}
						onChangeYear={setSelectedYear}
						selectedYear={selectedYear}
					/>
				)}
				<SortSelect selectedSort={sortType} onChangeSort={setSortType} />
			</FilterBlockContainer>
			{indexes && chronologicalIndexes ? (
				<IndexTable
					indexes={indexes}
					chronologicalIndexes={chronologicalIndexes}
				/>
			) : (
				<Loader />
			)}
			{podList && <AvailablePods podList={podList} />}
			<BackButton />
		</ScreenContainer>
	);
};

export default MeterInfos;
