import React, { FunctionComponent } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import logo from "../assets/images/ekwateur_logo.png";
import ScreenContainer from "../components/ScreenContainer";
import Title from "../components/Title";
import Text from "../components/Text";

const LogoImage = styled.img`
	width: 400px;
	@media (max-width: 600px) {
		width: 90%;
	}
`;

const MeterButton = styled.div`
	display : flex:
	align-items : center;
	justifycontent : center;
	background-color: ${({ theme }) => theme.colors.secondary};
	padding : 4px 24px;
	border-radius : ${({ theme }) => theme.radii.xl}px
`;

const MeterButtonText = styled(Text)`
	color: ${({ theme }) => theme.colors.white};
	font-weight: bold;
	font-size: 16px;
`;

const StyledLink = styled(Link)`
	text-decoration: none;
`;

const Welcome: FunctionComponent = () => {
	return (
		<ScreenContainer>
			<LogoImage src={logo} alt="ekwateur-logo" />
			<Title>Bienvenue chez votre fournisseur d'énergie verte</Title>
			<StyledLink to="/meter-infos">
				<MeterButton>
					<MeterButtonText>Consulter ma consommation </MeterButtonText>
				</MeterButton>
			</StyledLink>
		</ScreenContainer>
	);
};

export default Welcome;
