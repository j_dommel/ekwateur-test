import { getAvailableYears, getSortedIndexes } from "../utils";

const mockPodIndexes = [
	{
		id: "1",
		meterId: "1",
		createdAt: "2017-04-20T07:25:10.587Z",
		indexHigh: 73457,
		meter: {
			id: "1",
			createdAt: "2019-04-20T21:43:42.981Z",
			pointOfDelivery: "226378926478926",
		},
	},
	{
		id: "2",
		meterId: "1",
		createdAt: "2018-04-21T17:32:21.365Z",
		indexHigh: 85260,
		meter: {
			id: "1",
			createdAt: "2019-04-20T21:43:42.981Z",
			pointOfDelivery: "226378926478926",
		},
	},
	{
		id: "3",
		meterId: "1",
		createdAt: "2019-04-22T03:09:05.185Z",
		indexHigh: 33143,
		meter: {
			id: "1",
			createdAt: "2019-04-20T21:43:42.981Z",
			pointOfDelivery: "226378926478926",
		},
	},
	{
		id: "4",
		meterId: "1",
		createdAt: "2020-04-22T03:09:05.185Z",
		indexHigh: 33145,
		meter: {
			id: "1",
			createdAt: "2019-04-20T21:43:42.981Z",
			pointOfDelivery: "226378926478926",
		},
	},
	{
		id: "5",
		meterId: "1",
		createdAt: "2020-04-22T03:09:05.185Z",
		indexHigh: 33146,
		meter: {
			id: "1",
			createdAt: "2019-04-20T21:43:42.981Z",
			pointOfDelivery: "226378926478926",
		},
	},
];

test("it should return available years", () => {
	expect(getAvailableYears(mockPodIndexes).length).toBe(4);
	expect(getAvailableYears(mockPodIndexes)[0]).toBe(2017);
});

test("it should sort indexes", () => {
	expect(getSortedIndexes("higherFirst", mockPodIndexes)[0].indexHigh).toBe(
		85260
	);
	expect(getSortedIndexes("lowerFirst", mockPodIndexes)[0].indexHigh).toBe(
		33143
	);
	expect(
		getSortedIndexes("lowerFirst", mockPodIndexes, 2017)[0].indexHigh
	).toBe(73457);
	expect(getSortedIndexes("olderFirst", mockPodIndexes)[0].indexHigh).toBe(
		73457
	);
});
