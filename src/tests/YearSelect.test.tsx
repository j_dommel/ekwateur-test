import { render, cleanup, fireEvent } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import theme from "../theme";
import YearSelect from "../components/YearSelect";
import React from "react";

afterEach(cleanup);

describe("Test Year select", () => {
	const mockYears = [2019, 2020, 2021];
	const mockChange = jest.fn();

	it("should display component", () => {
		const { queryAllByText } = render(
			<ThemeProvider theme={theme}>
				<YearSelect
					availableYears={mockYears}
					selectedYear={mockYears[0]}
					onChangeYear={mockChange}
				/>
			</ThemeProvider>
		);

		const SelectComponent = queryAllByText(/année/i);

		expect(SelectComponent).toBeDefined();
	});

	it("should display selectedYear", () => {
		const { getByText } = render(
			<ThemeProvider theme={theme}>
				<YearSelect
					availableYears={mockYears}
					selectedYear={mockYears[0]}
					onChangeYear={mockChange}
				/>
			</ThemeProvider>
		);

		const SelectComponent = getByText("2019");

		expect(SelectComponent).toBeDefined();
	});

	it("should call onChangeYear", () => {
		const { getByText, getByTestId } = render(
			<ThemeProvider theme={theme}>
				<YearSelect
					availableYears={mockYears}
					selectedYear={mockYears[0]}
					onChangeYear={mockChange}
				/>
			</ThemeProvider>
		);

		expect(getByText("2019")).toBeDefined();

		fireEvent.change(getByTestId("year-select"), {
			target: { value: "2020" },
		});

		expect(mockChange).toHaveBeenCalled();
		expect(getByText("2020")).toBeDefined();
	});
});
