import React from "react";
import { render } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import Text from "../components/Text";
import theme from "../theme";

test("renders Text component child", () => {
	const { getByText } = render(
		<ThemeProvider theme={theme}>
			<Text>My text</Text>
		</ThemeProvider>
	);
	const textElement = getByText(/My text/i);
	expect(textElement).toBeDefined();
});
