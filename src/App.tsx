import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import GlobalFonts from "./assets/fonts/font";
import MeterInfos from "./screens/MeterInfos";
import Welcome from "./screens/Welcome";
import theme from "./theme";

function App() {
	return (
		<ThemeProvider theme={theme}>
			<GlobalFonts />
			<Router>
				<Route exact path="/" component={Welcome} />
				<Route path="/meter-infos" component={MeterInfos} />
			</Router>
		</ThemeProvider>
	);
}

export default App;
